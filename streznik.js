var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	var oseba = req.query;
	var i = uporabnikiSpomin.length;
	uporabnikiSpomin[i] = {davcnaStevilka: oseba.davcnaStevilka, ime: oseba.ime, priimek: oseba.priimek, naslov: oseba.naslov, hisnaStevilka: oseba.hisnaStevilka, postnaStevilka: oseba.postnaStevilka, kraj: oseba.kraj, drzava: oseba.drzava, poklic: oseba.poklic, telefonskaStevilka: oseba.telefonskaStevilka}, 
	res.send('Potrebno je implementirati dodajanje oseb!');
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	var davcna = req.query.davcnaStevilka;
	if(davcna == "" || davcna == null){
		res.send('Napačna zahteva');
	}
	var pogoj = false;
	for(var i in uporabnikiSpomin){
		if(davcna == uporabnikiSpomin[i].davcnaStevilka){
			uporabnikiSpomin.splice(i, 1);
			res.redirect("/");
			pogoj = true;
		}
	}
	if(pogoj == false){
		
	}
	res.send( "Oseba z davčno številko"+davcna+ " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];